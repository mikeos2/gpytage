# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"
PYTHON_COMPAT=( python3_7 )

inherit distutils-r1

DESCRIPTION="GTK Utility to help manage Portage's user config files"
HOMEPAGE="https://gitlab.com/mikeos2/gpytage"
SRC_URI="https://gitlab.com/mikeos2/gpytage/-/raw/master/gpytage-0.4.0.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE=""

RDEPEND=">=dev-python/pygobject-3.34.0"
