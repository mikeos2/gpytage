#!/usr/bin/env python3

"""
    GPytage leftpanel.py module

    Fixed for Python 3 - August 2020 Michael Greene

    Copyright (C) 2008-2009 by Kenneth Prugh
    ken69267@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import logging
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gpytage.datastore import F_NAME, F_REF, folderModel
from gpytage import PackageFileObj
from gpytage import FolderObj
from gpytage.newFile import newFile
from gpytage.deleteFile import deleteFile
from gpytage.rename import renameFile
from gpytage.windowid import WindowID
from gpytage.fileOperations import saveModifiedFile
from gpytage.fileOperations import saveasModifiedFile

logger = logging.getLogger(__name__)


class LeftView(Gtk.TreeView,WindowID):

    def __init__(self, rwindow):
        """ Initialize """
        Gtk.TreeView.__init__(self)
        WindowID.__init__(self)

        self.set_model(folderModel)
        self.set_search_column(F_NAME)
        self.rwindow = rwindow

        # TreeViewColumns
        namecol = Gtk.TreeViewColumn('Package File')

        cell = Gtk.CellRendererText()

        # add CellRenderer to TreeViewColumn
        namecol.pack_start(cell, True)
        namecol.add_attribute(cell, 'text', F_NAME)
        namecol.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)

        self.append_column(namecol)

        # Allows us to check if the user simply clicked on the same file or on another one
        self.__lastSelected = None

        # Signals
        self.connect("cursor-changed", self.__clicked)
        self.connect("button_press_event", self.__rightClicked)
        logger.debug("LEFTVIEW: initialization complete.")

    def expandRows(self, *args):
        """ Expand all columns in the left panel """
        self.expand_all()

    def collapseRows(self, *args):
        """ Collapse all columns in the left panel """
        self.collapse_all()

    def __clicked(self, treeview, *args):
        """ Handle TreeView clicks """
        # global __lastSelected
        model, iter = treeview.get_selection().get_selected()
        if iter:  # None if no row is selected
            target = model.get_value(iter, F_REF)
            targetName = target.getPath()
        else:
            targetName = self.__lastSelected
        # Has the selection changed
        logger.debug("LEFTVIEW:  Selected:  %s", targetName)
        if targetName != self.__lastSelected:
            logger.debug("LEFTVIEW: parent change detected %s", type(target))
            if isinstance(target, PackageFileObj.PackageFileObj):  # A file
                logger.debug("LEFTVIEW: attempting to change to:  %s", target.getPath())
                self.rwindow.setListModel(target.getData())
            elif isinstance(target, FolderObj.FolderObj):  # A folder
                logger.debug("LEFTVIEW:  Selected folder")
                self.rwindow.setListModel(None)
        # save current selection as last selected
        self.__lastSelected = targetName

    def __rightClicked(self, view, event):
        """ Right click menu for file options """
        if event.button == 3:
            menu = Gtk.Menu()
            new = Gtk.MenuItem("New File")
            new.connect("activate", newFile)
            menu.append(new)
            new = Gtk.MenuItem("Save")
            new.connect("activate", saveModifiedFile)
            menu.append(new)
            new = Gtk.MenuItem("Save As")
            new.connect("activate", saveasModifiedFile) # ToDo need save as
            menu.append(new)
            rename = Gtk.MenuItem("Rename File")
            rename.connect("activate", renameFile)
            menu.append(rename)
            delete = Gtk.MenuItem("Delete File")
            delete.connect("activate", deleteFile)
            menu.append(delete)
            menu.show_all()
            menu.popup(None, None, None, None, event.button, event.time)
