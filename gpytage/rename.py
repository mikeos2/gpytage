#!/usr/bin/env python3

"""
    rename.py GPytage module

    Fixed for Python 3 - August 2020 Michael Greene

    Copyright (C) 2008-2009 by Kenneth Prugh
    ken69267@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import os
from os import rename
import sys
import logging
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gpytage.config import get_config_path
from gpytage.config import config_files
from gpytage.fileOperations import hasModified
from gpytage.PackageFileObj import PackageFileObj
from gpytage.FolderObj import FolderObj
from gpytage.datastore import addedPaths
from gpytage.datastore import reinitializeDatabase
from gpytage.windowid import WindowID

logger = logging.getLogger(__name__)


def renameFile(*args):
    """ Renames the currently selected file """
    model, iter = WindowID.leftpanel.get_selection().get_selected()
    from gpytage.datastore import F_REF
    try:
        object = model.get_value(iter, F_REF)
        if isinstance(object, PackageFileObj):  # A file
            type = "File"
            if object.getParentFolder() == None:
                setFolder = get_config_path()
            else:
                setFolder = object.getParentFolder().getPath()
        elif isinstance(object, FolderObj):  # A folder
            type = "Directory"
            setFolder = object.getParentFolder().getPath()
        if __ensureNotModified():
            __createRenameDialog(object, type, setFolder)
    except TypeError as e:
        print("__renameFile:", e, file=sys.stderr)


def __createRenameDialog(object, type, setFolder):
    """ Spawms the Rename Dialog where a user can choose what the file should
    be renamed to """
    fc = Gtk.FileChooserDialog("Rename file...",
                               WindowID.windowID,
                               Gtk.FileChooserAction.SAVE,
                               (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                Gtk.STOCK_SAVE, Gtk.ResponseType.ACCEPT))
    fc.set_do_overwrite_confirmation(True)
    fc.set_filename(object.getPath())  # Set the fc to the object to be renamed
    fc.set_extra_widget(Gtk.Label("Renaming " + object.getPath()))
    response = fc.run()
    if response == Gtk.ResponseType.ACCEPT:
        if fc.get_filename() != None:
            __writeRenamedFile(object.getPath(), fc.get_filename())
            if object.saveasFile:
                for item in addedPaths:
                    for oldFile in config_files:
                        if oldFile is item[1]:
                            config_files.remove(item[1])
                    addedPaths.remove(item)
                    head_tail = os.path.split(fc.get_filename())
                    addedPaths.append(head_tail)
                    config_files.append(head_tail[1])
            reinitializeDatabase()
            __reselectAfterRename(fc.get_filename(), type)
        else:
            print("Invalid rename request", file=sys.stderr)
    fc.destroy()


def __writeRenamedFile(oldFile, newFile):
    """ Performs the actual renaming of the file """
    try:
        rename(oldFile, newFile)
        logger.debug("%s renamed to %s", oldFile, newFile)
    except IOError as e:
        print("Rename: ", e, file=sys.stderr)


def __ensureNotModified():
    """ Ensure no files have been modified before proceeding, returns True if
    nothing is modified """
    if hasModified():
        # inform user to save
        Gtk.MessageDialog(WindowID.windowID,
                          Gtk.DialogFlags.DESTROY_WITH_PARENT,
                          Gtk.MessageType.ERROR,
                          Gtk.ButtonsType.OK,
                          "Unsaved Files Found...",
                          "A file cannot be renamed with unsaved changes. Please save your changes.")
        return False
    else:
        return True


def __reselectAfterRename(filePath, type):
    """ Reselects the parent folder of the deleted object """
    model = WindowID.leftpanel.get_model()
    model.foreach(getMatch, [filePath, WindowID.leftpanel, type])


def getMatch(model, path, iter, data):
    """ Obtain the match and perform the selection """
    testObject = model.get_value(iter, 1)  # col 1 stores the reference
    # clarify values passed in from data list
    filePath = data[0]
    leftview = data[1]
    type = data[2]
    if testObject.getPath() == filePath:
        # We found the file object we just renamed, lets select it
        leftview.expand_to_path(path)
        leftview.set_cursor(path, None, False)
        return True
    else:
        return False
